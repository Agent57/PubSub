#include "LogEventHandler.h"

LogEventHandler::LogEventHandler()
{
  m_level = Info;
}

void LogEventHandler::OutputLogEvent(const LogEventData& event)
{
  if (event.Level > m_level)
    return;

  auto text = m_formatter->FormatLogOutput(event);
  m_writer->WriteLogEvent(text);
}

void LogEventHandler::SetLogFormatter(LogFormatterPtr formatter)
{
  m_formatter = move(formatter);
}

void LogEventHandler::SetLogWriter(LogWriterPtr writer)
{
  m_writer = move(writer);
}

bool LogEventHandler::SetLogOutputLevel(LogLevel level)
{
  if (m_level == level || m_level == Internal)
    return false;

  m_level = level;
  return true;
}
