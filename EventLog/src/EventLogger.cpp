#include <windows.h>

#include "Logging.h"
#include "IDELogger.h"

#include <chrono>
#include <iomanip>

EventLogger::EventLogger()
{
  m_queue = std::make_unique<LogEventQueue>();
  m_timer = std::make_unique<TimeKeeper>();
  m_enabled = true;
  m_max_level = Info;
  m_running = false;
}

EventLogger::~EventLogger()
{
  if (!m_running)
    return;

  LogEvent(Internal, "Application logging complete");
  m_running = false;

  if (m_logger.joinable())
    m_logger.join();

  ProcessLogEvents(m_queue);
}

void EventLogger::Start()
{
  if (m_running)
    return;

  m_running = true;
  m_timer->Reset();
  LogEvent(Internal, "Application logging started");

  SetDebuggerLogging();

  m_logger = std::thread(&EventLogger::LogWorker, this);
}

void EventLogger::LogWorker()
{
  while (m_running)
  {
    if (std::cv_status::timeout == WaitForQueuedEvent())
      continue;

    auto buffer = BufferEventQueue();
    ProcessLogEvents(buffer);
  }
}

std::cv_status EventLogger::WaitForQueuedEvent()
{
  auto result = std::cv_status::no_timeout;
  if (m_queue->empty())
  {
    std::unique_lock<std::mutex> lock(m_lock);
    result = m_conditional.wait_for(lock, std::chrono::milliseconds(500));
  }

  return result;
}

EventLogger::LogEventQueuePtr EventLogger::BufferEventQueue()
{
  std::lock_guard<std::mutex> lock(m_lock);
  auto buffer = move(m_queue);
  m_queue = std::make_unique<LogEventQueue>();
  return buffer;
}

void EventLogger::ProcessLogEvents(const LogEventQueuePtr& events)
{
  while (!events->empty())
  {
    const auto& pLog = events->front();
    CallLogHandlers(*pLog);
    events->pop_front();
  }
}

void EventLogger::CallLogHandlers(const LogEventData& event)
{
  std::lock_guard<std::mutex> lock(m_handlerLock);
  for (auto& handler : m_handlers)
  {
    handler->OutputLogEvent(event);
  }
}

void EventLogger::SetDebuggerLogging()
{
  if (IsDebuggerPresent())
  {
    auto handler = std::make_unique<LogEventHandler>();
    handler->SetLogWriter(std::make_unique<IDELogWriter>());
    handler->SetLogFormatter(std::make_unique<IDELogFormatter>());
    m_handlers.push_back(move(handler));
  }
}

std::string EventLogger::ElapsedTime()
{
  // Convert the internal log time value into regular clock time
  auto runtime = m_timer->Elapsed();
  int milliseconds = runtime % 1000;
  int seconds = runtime / 1000 % 60;
  int minutes = runtime / 60000 % 60;
  int hours = runtime / 3600000 % 24;
  auto days = runtime / 86400000;

  // Output the time in an easily readable format
  std::stringstream ss;
  ss << std::setfill('0') << std::setw(3) << days << ":"
    << std::setw(2) << hours << ":"
    << std::setw(2) << minutes << ":"
    << std::setw(2) << seconds << ":"
    << std::setw(3) << milliseconds;
  return ss.str();
}

void EventLogger::QueueLogEvent(const LogEventData& pLog)
{
  std::lock_guard<std::mutex> lock(m_lock);
  m_queue->push_back(std::make_unique<LogEventData>(pLog));
  m_conditional.notify_all();
}

void EventLogger::SetLogLevel(LogLevel level)
{
  bool result;

  for (auto& handler : m_handlers)
  {
    result |= handler->SetLogOutputLevel(level);
  }

  if (result)
    LogEvent(Internal, "Log output level set to " << LogEventData::Severity(level));
}

void EventLogger::SetEnabled(bool enable)
{
  if (m_enabled == enable)
    return;

  m_enabled = enable;
  LogEvent(Internal, "EventLogger output is now " << (m_enabled ? "enabled" : "disabled"));
}

bool EventLogger::IsEnabled() const
{
  return m_enabled;
}

void EventLogger::AttachHandler(LogFormatterPtr formatter, LogWriterPtr writer)
{
  auto handler = std::make_unique<LogEventHandler>();
  handler->SetLogFormatter(move(formatter));
  handler->SetLogWriter(move(writer));
  std::lock_guard<std::mutex> lock(m_handlerLock);
  m_handlers.push_back(move(handler));
  Start();
}

bool EventLogger::CheckLogAccess(LogLevel level) const
{
  return m_enabled && m_running;
}

void EventLogger::SetTimeKeeper(TimeKeeperPtr timer)
{
  m_timer = move(timer);
}
