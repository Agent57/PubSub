#pragma once

#include "IEventLogger.h"

#include <atomic>
#include <list>
#include <mutex>
#include <thread>
#include "TimeKeeper.h"

class EventLogger : public IEventLogger
{
  typedef std::list<LogEventDataPtr> LogEventQueue;
  typedef std::unique_ptr<LogEventQueue> LogEventQueuePtr;

  std::mutex m_lock;
  std::mutex m_handlerLock;
  std::condition_variable m_conditional;
  LogEventQueuePtr m_queue;
  TimeKeeperPtr m_timer;
  std::atomic_bool m_enabled;
  std::atomic_bool m_running;
  std::atomic<LogLevel> m_max_level;
  std::thread m_logger;
  std::list<LogEventHandlerPtr> m_handlers;

  EventLogger(EventLogger const& copy);
  EventLogger& operator= (EventLogger const& copy);
  void Start();
  void LogWorker();
  std::cv_status WaitForQueuedEvent();
  LogEventQueuePtr BufferEventQueue();
  void ProcessLogEvents(const LogEventQueuePtr& events);
  void CallLogHandlers(const LogEventData& event);
  void SetDebuggerLogging();

public:
  EventLogger();
  ~EventLogger();

  std::string ElapsedTime() override;
  void QueueLogEvent(const LogEventData& pLog) override;
  void SetLogLevel(LogLevel level) override;
  void SetEnabled(bool enable) override;
  bool IsEnabled() const override;
  void AttachHandler(LogFormatterPtr formatter, LogWriterPtr writer) override;
  bool CheckLogAccess(const LogLevel level) const override;
  void SetTimeKeeper(TimeKeeperPtr timer);
};
