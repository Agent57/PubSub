#pragma once

#include "LogEventData.h"
#include "LogEventHandler.h"

#include <memory>


class IEventLogger
{
public:
  virtual ~IEventLogger() {}

  virtual std::string ElapsedTime() = 0;
  virtual void QueueLogEvent(const LogEventData& pLog) = 0;
  virtual void SetLogLevel(LogLevel level) = 0;
  virtual void SetEnabled(bool enable) = 0;
  virtual bool IsEnabled() const = 0;
  virtual void AttachHandler(LogFormatterPtr formatter, LogWriterPtr writer) = 0;
  virtual bool CheckLogAccess(const LogLevel level) const = 0;
};
typedef std::unique_ptr<IEventLogger> EventLoggerPtr;
