#pragma once

#include "LogEventData.h"
#include "ILogFormatter.h"
#include "ILogWriter.h"

#include <memory>

class LogEventHandler
{
  LogLevel m_level;
  LogWriterPtr m_writer;
  LogFormatterPtr m_formatter;

public:
  LogEventHandler();

  void OutputLogEvent(const LogEventData& event);
  void SetLogFormatter(LogFormatterPtr formatter);
  void SetLogWriter(LogWriterPtr writer);
  bool SetLogOutputLevel(LogLevel level);
};
typedef std::unique_ptr<LogEventHandler> LogEventHandlerPtr;