#pragma once

#include "EventLogger.h"

#include <sstream>

#define LogEvent(level, text) { \
  if (Logging::CheckLogAccess(level)) \
    { \
    std::ostringstream ss; \
    ss << text; \
    LogEventData pLog(level, Logging::ElapsedTime(), ss.str(), __FILE__, __FUNCTION__, __LINE__); \
    Logging::QueueLogEvent(pLog); \
    } \
}

class Logging
{
  EventLoggerPtr m_logging;

  Logging()
  {
    m_logging = std::make_unique<EventLogger>();
  }

  Logging(Logging const& copy);
  Logging& operator= (Logging const& copy);

  ~Logging() {}

  static Logging& Instance()
  {
    static Logging instance;
    return instance;
  }

public:
  static std::string ElapsedTime()
  {
    return Instance().m_logging->ElapsedTime();
  }

  static void QueueLogEvent(const LogEventData& pLog)
  {
    Instance().m_logging->QueueLogEvent(pLog);
  }

  static void SetLogLevel(const LogLevel level)
  {
    Instance().m_logging->SetLogLevel(level);
  }

  static void SetEnabled(bool enable)
  {
    Instance().m_logging->SetEnabled(enable);
  }

  static bool IsEnabled()
  {
    return Instance().m_logging->IsEnabled();
  }

  static void AttachHandler(LogFormatterPtr formatter, LogWriterPtr writer)
  {
    Instance().m_logging->AttachHandler(move(formatter), move(writer));
  }

  static bool CheckLogAccess(LogLevel level)
  {
    return Instance().m_logging->CheckLogAccess(level);
  }
};