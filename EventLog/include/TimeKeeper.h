#pragma once

#include <chrono>

class ITimeKeeper
{
public:
  virtual ~ITimeKeeper() {}
  virtual void Reset() = 0;
  virtual long long Elapsed() const = 0;
};
typedef std::unique_ptr<ITimeKeeper> TimeKeeperPtr;


class TimeKeeper : public ITimeKeeper
{
protected:
  using clock = std::chrono::high_resolution_clock;
  using milliseconds = std::chrono::milliseconds;

  virtual clock::time_point now() const
  {
    return clock::now();
  }

private:
	std::chrono::time_point<clock> m_start;

public:
  TimeKeeper()
  {
    m_start = clock::now();
  }

  void Reset() override
  {
    m_start = now();
  }

  long long Elapsed() const override
  {
	  return std::chrono::duration_cast<milliseconds>(now() - m_start).count();
  }
};