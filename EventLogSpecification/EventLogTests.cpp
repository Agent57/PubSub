#include "CppUnitTest.h"

#include "EventLogger.h"
#include "IDELogger.h"
#include "ConsoleLogger.h"

#include <iomanip>
#include "TimeKeeper.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace EventLogSpecification
{
  typedef std::list<LogEventDataPtr> LogEventQueue;

  class FakeLogWriter : public ILogWriter
  {
  public:
    FakeLogWriter() {}
    void WriteLogEvent(std::string) override {}
  };

  class FakeLogFormatter : public ILogFormatter
  {
    LogEventQueue *Events;

  public:
    explicit FakeLogFormatter(LogEventQueue *events) :
      Events(events)
    {
    }

    std::string FormatLogOutput(const LogEventData& event) override
    {
      Events->push_back(std::make_unique<LogEventData>(event));
      return event.Text;
    }
  };
  
  class FakeTimeKeeper : public TimeKeeper
  {
    clock::time_point m_elapsed;

  public:
    explicit FakeTimeKeeper(int timewarp)
    {
      m_elapsed = clock::now();
      Reset();
      AlterTime(timewarp);
    }

    clock::time_point now() const override
    {
      return m_elapsed;
    }

    void AlterTime(int ms)
    {
      m_elapsed += std::chrono::milliseconds(ms);
    }
  };

  TEST_CLASS(EventLogTests)
  {
    std::unique_ptr<LogEventQueue> Events;
    EventLogger logger;

  public:
    TEST_METHOD_INITIALIZE(Initialise)
    {
      Events = std::make_unique<LogEventQueue>();
    }

    TEST_METHOD(Check_IDE_logger_output_format)
    {
      IDELogFormatter formatter;
      LogEventData event(Debug, "12:34:567", "Text", "Path\\Filename", "unused", 123);

      auto result = formatter.FormatLogOutput(event);

      std::stringstream expected;
      expected << "Path\\Filename(123): 12:34:567 [" << event.ThreadId << "] Debug: Text" << std::endl;
      Assert::AreEqual(expected.str().c_str(), result.c_str());
    }

    TEST_METHOD(Check_console_logger_output_format)
    {
      ConsoleLogFormatter formatter;
      LogEventData event(Debug, "12:34:567", "Text", "Path\\Filename", "unused", 123);

      auto result = formatter.FormatLogOutput(event);

      std::stringstream expected;
      expected << "12:34:567  " << std::setfill('0') << std::setw(5) << event.ThreadId << "  Debug    Text" << std::endl;
      Assert::AreEqual(expected.str().c_str(), result.c_str());
    }

    TEST_METHOD(The_elapsed_time_since_initialisation_is_formatted_correctly)
    {
      std::string expected = "001:02:03:04:005";
      FakeTimeKeeper timer(93784005);

      logger.SetTimeKeeper(std::make_unique<FakeTimeKeeper>(timer));
      auto result = logger.ElapsedTime();

      Assert::AreEqual(expected.c_str(), result.c_str());
    }

    TEST_METHOD(TimeKeeper_counts_the_number_of_milliseconds_since_initialisation)
    {
      FakeTimeKeeper timer(100);

      auto result = int(timer.Elapsed());

      Assert::AreEqual(100, result);
    }

    TEST_METHOD(TimeKeeper_Reset_initialises_the_clock_to_start_again)
    {
      FakeTimeKeeper timer(100);

      timer.Reset();
      auto result = int(timer.Elapsed());

      Assert::AreEqual(0, result);
    }

    TEST_METHOD(Calls_a_handler_for_a_queued_event)
    {
      AttachFakeHandler(logger);
      LogEventData log(Info, "AAA", "BBB", "CCC", "DDD", 123);

      logger.QueueLogEvent(log);
      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      Assert::AreEqual(1, int(Events->size()));
      Assert::IsTrue(log == GetEventResult());
    }

    TEST_METHOD(Calls_a_handler_for_each_queued_event)
    {
      AttachFakeHandler(logger);
      LogEventData log1(Info, "AAA", "BBB", "CCC", "DDD", 123);
      LogEventData log2(Info, "www", "xxx", "yyy", "zzz", 7890);

      logger.QueueLogEvent(log1);
      logger.QueueLogEvent(log2);
      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      Assert::AreEqual(2, int(Events->size()));
      Assert::IsTrue(log1 == GetEventResult());
      Assert::IsTrue(log2 == GetEventResult());
    }

    TEST_METHOD(Calls_all_handlers_for_each_queued_event)
    {
      AttachFakeHandler(logger);
      AttachFakeHandler(logger);
      LogEventData log1(Info, "AAA", "BBB", "CCC", "DDD", 123);
      LogEventData log2(Info, "www", "xxx", "yyy", "zzz", 7890);

      logger.QueueLogEvent(log1);
      logger.QueueLogEvent(log2);
      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      Assert::AreEqual(4, int(Events->size()));
      Assert::IsTrue(log1 == GetEventResult());
      Assert::IsTrue(log1 == GetEventResult());
      Assert::IsTrue(log2 == GetEventResult());
      Assert::IsTrue(log2 == GetEventResult());
    }

    void AttachFakeHandler(EventLogger& logger)
    {
      auto formatter = std::make_unique<FakeLogFormatter>(Events.get());
      auto writer = std::make_unique<FakeLogWriter>();
      logger.AttachHandler(move(formatter), move(writer));
    }

    LogEventData GetEventResult()
    {
      auto result = *Events->front().get();
      Events->pop_front();
      return result;
    }
  };
}
