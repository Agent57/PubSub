#include "CppUnitTest.h"
#include <list>
#include <LogEventData.h>
#include <TimeKeeper.h>
#include <mutex>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace EventLogSpecification
{
  class ILogHandler
  {
  public:
    virtual ~ILogHandler() {}
    virtual void CallLogHandlers(const LogEventDataPtr& data) const = 0;
  };

  class FakeLogHandler : public ILogHandler
  {
  public:
    int delay = 0;
    mutable int Called = 0;

    void CallLogHandlers(const LogEventDataPtr& data) const override
    {
      Called++;
      if (delay)
        std::this_thread::sleep_for(std::chrono::milliseconds(delay));
    }
  };

  class EventLogQueue
  {
    typedef std::list<LogEventDataPtr> LogEventQueue;
    typedef std::unique_ptr<LogEventQueue> LogEventQueuePtr;

    LogEventQueuePtr m_queue = std::make_unique<LogEventQueue>();
    std::mutex m_lock;
    std::condition_variable m_conditional;

    LogEventQueuePtr BufferEventQueue()
    {
      std::lock_guard<std::mutex> lock(m_lock);
      auto buffer = move(m_queue);
      m_queue = std::make_unique<LogEventQueue>();
      return buffer;
    }

  public:
    bool QueuedEventAvailable()
    {
      std::unique_lock<std::mutex> lock(m_lock);
      auto predicate = [&]() { return !m_queue->empty(); };
      return m_conditional.wait_for(lock, std::chrono::milliseconds(250), predicate);
    }

    void QueueLogEvent(const LogEventData& pLog)
    {
      std::lock_guard<std::mutex> lock(m_lock);
      m_queue->push_back(std::make_unique<LogEventData>(pLog));
      m_conditional.notify_all();
    }

    void ProcessLogEvents(const ILogHandler& handler)
    {
      auto buffer = BufferEventQueue();
      for (auto iterator = buffer->begin(); iterator != buffer->end(); ++iterator )
      {
        handler.CallLogHandlers(*iterator);
      }
    }
  };

  TEST_CLASS(EventLogQueueTests)
	{
    EventLogQueue m_queue;
    TimeKeeper m_timer;
    LogEventData m_log{ Info, "AAA", "BBB", "CCC", "DDD", 123 };

  public:
    TEST_METHOD(QueuedEventAvailable_returns_true_if_any_events_are_already_queued)
    {
      m_queue.QueueLogEvent(m_log);
      TimeKeeper timer;

      Assert::IsTrue(m_queue.QueuedEventAvailable());
    }

    TEST_METHOD(QueuedEventAvailable_will_block_for_a_short_time_and_return_false_if_no_events_are_queued)
    {
      Assert::IsFalse(m_queue.QueuedEventAvailable());
      Assert::IsTrue(m_timer.Elapsed() >= 250);
    }

    TEST_METHOD(QueuedEventAvailable_will_stop_blocking_and_return_true_if_events_are_queued)
    {
      bool result;

      std::thread testThread([&]() { result = m_queue.QueuedEventAvailable(); });
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
      m_queue.QueueLogEvent(m_log);
      testThread.join();
      auto timeTaken = int(m_timer.Elapsed());

      Assert::IsTrue(result);
      Assert::IsTrue(timeTaken >= 50 && timeTaken < 250);
    }

    TEST_METHOD(ProcessLogEvents_will_not_call_the_log_handlers_for_an_empty_queue)
    {
      FakeLogHandler handler;

      m_queue.ProcessLogEvents(handler);

      Assert::AreEqual(0, handler.Called);
    }

    TEST_METHOD(ProcessLogEvents_will_call_the_log_handlers_for_a_queued_event)
    {
      FakeLogHandler handler;

      m_queue.QueueLogEvent(m_log);
      m_queue.ProcessLogEvents(handler);

      Assert::AreEqual(1, handler.Called);
    }

    TEST_METHOD(ProcessLogEvents_removes_processed_items_from_the_queue)
    {
      FakeLogHandler handler;

      m_queue.QueueLogEvent(m_log);
      m_queue.ProcessLogEvents(handler);

      Assert::IsFalse(m_queue.QueuedEventAvailable());
    }

    TEST_METHOD(ProcessLogEvents_will_not_process_events_queued_after_its_called)
    {
      FakeLogHandler handler;
      handler.delay = 100;
      m_queue.QueueLogEvent(m_log);

      std::thread testThread([&]() { m_queue.ProcessLogEvents(handler); });
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
      m_queue.QueueLogEvent(m_log);
      testThread.join();

      Assert::AreEqual(1, handler.Called);
    }

    TEST_METHOD(Events_queued_whilst_processing_buffered_events_will_be_available_once_processing_completes)
    {
      FakeLogHandler handler;
      handler.delay = 100;
      m_queue.QueueLogEvent(m_log);

      std::thread testThread([&]() { m_queue.ProcessLogEvents(handler); });
      std::this_thread::sleep_for(std::chrono::milliseconds(50));
      m_queue.QueueLogEvent(m_log);
      testThread.join();

      Assert::IsTrue(m_queue.QueuedEventAvailable());
    }
  };
}