// TestApp.cpp : Defines the entry point for the console application.
//

#include "Logging.h"

#include "StringParameter.pb.h"

#include "FrameworkManager.h"
#include "Broker.h"
#include "MessageLoopRunner.h"
#include "ConsoleLogger.h"

#include <iostream>
#include <StringParameterWrapper.h>


int main(int argc, char** argv)
{
  Logging::AttachHandler(std::make_unique<ConsoleLogFormatter>(), std::make_unique<ConsoleLogWriter>());
  Logging::SetLogLevel(Trace);
  LogEvent(Info, "Application Running...");

  // Initialise the application framework
  auto SystemBroker = std::make_shared<Broker>();
  auto SystemQueue = std::make_shared<QueueConnector>();
  auto DefaultMessageLoopRunner = std::make_shared<MessageLoopRunner>();
  auto SystemFramework = std::make_shared<FrameworkManager>(SystemBroker, SystemQueue, DefaultMessageLoopRunner);
  SystemFramework->Initialise();

  auto SystemQueueY = std::make_shared<QueueConnector>();
  auto DefaultMessageLoopRunnerY = std::make_shared<MessageLoopRunner>();
  auto SystemFrameworkY = std::make_shared<FrameworkManagerY>(SystemBroker, SystemQueueY, DefaultMessageLoopRunnerY);
  SystemFrameworkY->Initialise();

  auto TimeoutMsg = StringParameterWrapper::Create();
  TimeoutMsg->Message().set_data("Hello World");
  SystemFrameworkY->SetTimer(1500, TimeoutMsg, true);

  // Pause the console waiting on user input
  LogEvent(Info, "Press <Enter> key to exit...");
  std::cin.ignore();
  LogEvent(Info, "Application Shutting down");


  return 0;
}
