#include "RemoteSubscriptionVisitor.h"
#include "StringParameterWrapper.h"

#include <sstream>

bool RemoteSubscriptionVisitor::Visit(const Exchange::RemoteSubscription &message)
{
  auto subscriptionId = message.subscriptionid();
  m_domain.lock()->SetTimer(1250, nullptr);
  
  std::ostringstream stringStream;
  stringStream << "Subscription Id: " << subscriptionId;
  
  auto msg = StringParameterWrapper::Create();
  msg->Message().set_data(stringStream.str());
  
  m_domain.lock()->Send(msg);

  return true;
}

std::shared_ptr<IRemoteSubscriptionVisitor> RemoteSubscriptionVisitor::Create(const DomainManagerPtr& domain)
{
  return std::make_shared<RemoteSubscriptionVisitor>(domain);
}