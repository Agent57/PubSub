#pragma once

#include "IRemoteSubscriptionVisitor.h"
#include "IDomainManager.h"

// Message handler
class RemoteSubscriptionVisitor : public IRemoteSubscriptionVisitor
{
  const std::weak_ptr<IDomainManager> m_domain;

public:
  explicit RemoteSubscriptionVisitor(const DomainManagerPtr& domain) : m_domain(domain) {}
  bool Visit(const Exchange::RemoteSubscription &message) override;
  static std::shared_ptr<IRemoteSubscriptionVisitor> Create(const DomainManagerPtr& domain);
};