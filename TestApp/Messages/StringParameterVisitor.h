#pragma once

#include "IStringParameterVisitor.h"
#include "IDomainManager.h"

// Message handler
class StringParameterVisitor : public IStringParameterVisitor
{
  const std::weak_ptr<IDomainManager> m_domain;

public:
  explicit StringParameterVisitor(const DomainManagerPtr& domain) : m_domain(domain) {}
  bool Visit(const Exchange::StringParameter &message) override;
  static std::shared_ptr<IStringParameterVisitor> Create(const DomainManagerPtr& domain);
};