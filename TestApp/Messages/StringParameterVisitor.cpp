#include "StringParameterVisitor.h"

#include "Logging.h"
#include <RemoteSubscription.pb.h>
#include "RemoteSubscriptionWrapper.h"

bool StringParameterVisitor::Visit(const Exchange::StringParameter &message)
{
  static int x = 0;
  LogEvent(Debug, "Message content... [" << message.data() << "]");
  auto msg = RemoteSubscriptionWrapper::Create();
  msg->Message().set_subscriptionid(x++);

  if (message.data() == "Hello World")
    m_domain.lock()->Send(msg);

  return true;
}

std::shared_ptr<IStringParameterVisitor> StringParameterVisitor::Create(const DomainManagerPtr& domain)
{
  return std::make_shared<StringParameterVisitor>(domain);
}