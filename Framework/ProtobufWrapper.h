#pragma once

#include <google/protobuf/message.h>
#include "ISerialiser.h"

class ProtobufWrapper : public Iserialiser
{
public:
  DataBufferPtr Serialise(const ProtobufMessagePtr& msg) override;
  ProtobufMessagePtr Deserialise(const DataBufferPtr& buffer) override;
};
