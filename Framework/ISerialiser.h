#pragma once

#include <memory>
#include <vector>
#include <google/protobuf/message.h>

typedef std::vector<char> DataBuffer;
typedef std::shared_ptr<DataBuffer> DataBufferPtr;
typedef std::shared_ptr<::google::protobuf::Message> ProtobufMessagePtr;

class Iserialiser
{
public:
  virtual ~Iserialiser(void) {};
  virtual DataBufferPtr Serialise(const ProtobufMessagePtr& msg) = 0;
  virtual ProtobufMessagePtr Deserialise(const DataBufferPtr& buffer) = 0;
};

typedef std::shared_ptr<Iserialiser> SerialiserPtr;
