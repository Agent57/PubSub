#include "TestMessageVisitor.h"

TestMessageVisitor::TestMessageVisitor(const DomainManagerPtr& domain) : m_domain(domain)
{
  ProcessCalled = 0;
  ProcessExpectedResult = false;

  SavedDomainManager = domain;
}


bool TestMessageVisitor::Visit(const Exchange::TestMessage &message)
{
  ProcessCalled++;
  SavedMessage = message;
  return ProcessExpectedResult;
}

std::shared_ptr<TestMessageVisitor> TestMessageVisitor::Create(const DomainManagerPtr& domain)
{
  return std::make_shared<TestMessageVisitor>(domain);
}