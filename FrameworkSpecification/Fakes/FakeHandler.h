#pragma once

#include "IDomainManager.h"
#include "IHandler.h"

#include <TestMessageWrapper.h>

namespace FrameworkSpecification
{
  class FakeHandler : public IHandler
  {
  public:
    int ProcessCalled;
    bool ProcessExpectedResult;

    std::weak_ptr<IDomainManager> SavedDomainManager;
    std::shared_ptr<TestMessageWrapper const> SavedMessage;

    explicit FakeHandler(const DomainManagerPtr& domain)
    {
      ProcessCalled = 0;
      ProcessExpectedResult = false;

      SavedDomainManager = domain;
      SavedMessage = nullptr;
    }

    bool Visit(const  std::shared_ptr<TestMessageWrapper const>& pMsg)
    {
      ProcessCalled++;
      SavedMessage = pMsg;
      return ProcessExpectedResult;
    }
  };

  typedef std::shared_ptr<FakeHandler> FakeHandlerPtr;
}