#pragma once

#include "ITestMessageVisitor.h"
#include "IDomainManager.h"

// Message handler
class TestMessageVisitor : public ITestMessageVisitor
{
  const std::weak_ptr<IDomainManager> m_domain;

public:
  int ProcessCalled;
  bool ProcessExpectedResult;

  std::weak_ptr<IDomainManager> SavedDomainManager;
  Exchange::TestMessage SavedMessage;

  explicit TestMessageVisitor(const DomainManagerPtr& domain);
  bool Visit(const Exchange::TestMessage &message) override;
  static std::shared_ptr<TestMessageVisitor> Create(const DomainManagerPtr& domain);
};