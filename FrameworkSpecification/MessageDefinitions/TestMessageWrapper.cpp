
#include "IHandler.h"
#include "TestMessageWrapper.h"
#include "ITestMessageVisitor.h"

// Boilerplate message wrapper
bool TestMessageWrapper::CallHandler(HandlerPtr visitor) const
{
  if (auto stringParameterVisitor = std::dynamic_pointer_cast<ITestMessageVisitor>(visitor))
    return stringParameterVisitor->Visit(m_message);
  else { /* Throw Error */ }

  return false;
}

std::string TestMessageWrapper::GetTypeName() const
{
  return m_message.GetTypeName();
}

Exchange::TestMessage& TestMessageWrapper::Message()
{
  return m_message;
}

std::shared_ptr<TestMessageWrapper> TestMessageWrapper::Create()
{
  return std::make_shared<TestMessageWrapper>();
}
