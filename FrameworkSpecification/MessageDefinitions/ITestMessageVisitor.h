#pragma once

#include "IHandler.h"
#include "TestMessage.pb.h"

// Message handler
class ITestMessageVisitor : public IHandler
{
public:
  virtual ~ITestMessageVisitor(){}
  virtual bool Visit(const Exchange::TestMessage &message) = 0;
};