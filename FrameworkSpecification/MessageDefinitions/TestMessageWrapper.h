#pragma once

#include "TestMessage.pb.h"
#include "IMessage.h"


class TestMessageWrapper : public IMessage
{
  Exchange::TestMessage m_message;

public:
  bool CallHandler(HandlerPtr visitor) const override;
  std::string GetTypeName() const override;
  Exchange::TestMessage& Message();
  static std::shared_ptr<TestMessageWrapper> Create();
};
