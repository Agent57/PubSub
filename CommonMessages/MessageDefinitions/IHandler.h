#pragma once

#include <memory>

class IHandler
{
public:
  virtual ~IHandler(void)  {}
};

typedef std::shared_ptr<IHandler> HandlerPtr;
