#pragma once

#include "StringParameter.pb.h"
#include "IMessage.h"


class StringParameterWrapper : public IMessage
{
  Exchange::StringParameter m_message;

public:
  bool CallHandler(HandlerPtr visitor) const override;
  std::string GetTypeName() const override;
  Exchange::StringParameter& Message();
  static std::shared_ptr<StringParameterWrapper> Create();
};
