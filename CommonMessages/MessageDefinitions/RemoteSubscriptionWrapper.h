#pragma once

#include "RemoteSubscription.pb.h"
#include "IMessage.h"


class RemoteSubscriptionWrapper : public IMessage
{
  Exchange::RemoteSubscription m_message;

public:
  bool CallHandler(HandlerPtr visitor) const override;
  std::string GetTypeName() const override;
  Exchange::RemoteSubscription& Message();
  static std::shared_ptr<RemoteSubscriptionWrapper> Create();
};
