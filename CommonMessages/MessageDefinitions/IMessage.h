#pragma once

#include "IHandler.h"
#include <string>

class IMessage
{
public:
  virtual ~IMessage() {}
  virtual bool CallHandler(HandlerPtr visitor) const = 0;
  virtual std::string GetTypeName() const = 0;
};
typedef std::shared_ptr<IMessage> MessagePtr;
