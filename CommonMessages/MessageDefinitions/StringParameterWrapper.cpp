#include "IHandler.h"
#include "StringParameterWrapper.h"
#include "IStringParameterVisitor.h"

// Boilerplate message wrapper
bool StringParameterWrapper::CallHandler(HandlerPtr visitor) const
{
  if (auto stringParameterVisitor = std::dynamic_pointer_cast<IStringParameterVisitor>(visitor))
    return stringParameterVisitor->Visit(m_message);
  else { /* Throw Error */ }

  return false;
}

std::string StringParameterWrapper::GetTypeName() const
{
  return m_message.GetTypeName();
}

Exchange::StringParameter& StringParameterWrapper::Message()
{
  return m_message;
}

std::shared_ptr<StringParameterWrapper> StringParameterWrapper::Create()
{
  return std::make_shared<StringParameterWrapper>();
}
