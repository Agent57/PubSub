#pragma once

#include "IHandler.h"
#include "RemoteSubscription.pb.h"

// Message handler
class IRemoteSubscriptionVisitor : public IHandler
{
public:
  virtual ~IRemoteSubscriptionVisitor(){}
  virtual bool Visit(const Exchange::RemoteSubscription &message) = 0;
};