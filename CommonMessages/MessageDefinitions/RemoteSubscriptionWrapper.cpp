#include "IHandler.h"
#include "RemoteSubscriptionWrapper.h"
#include "IRemoteSubscriptionVisitor.h"

std::string RemoteSubscriptionWrapper::GetTypeName() const
{
  return m_message.GetTypeName();
}

// Boilerplate message wrapper
bool RemoteSubscriptionWrapper::CallHandler(HandlerPtr visitor) const
{
  if (auto remoteSubscriptionVisitor = std::dynamic_pointer_cast<IRemoteSubscriptionVisitor>(visitor))
    return remoteSubscriptionVisitor->Visit(m_message);
  else { /* Throw Error */ }

  return false;
}

Exchange::RemoteSubscription& RemoteSubscriptionWrapper::Message()
{
  return m_message;
}

std::shared_ptr<RemoteSubscriptionWrapper> RemoteSubscriptionWrapper::Create()
{
  return std::make_shared<RemoteSubscriptionWrapper>();
}