#pragma once

#include "IHandler.h"
#include "StringParameter.pb.h"

// Message handler
class IStringParameterVisitor : public IHandler
{
public:
  virtual ~IStringParameterVisitor(){}
  virtual bool Visit(const Exchange::StringParameter &message) = 0;
};